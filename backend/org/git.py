import re

from github import Github
from IGitt.GitHub import GitHubToken
from IGitt.GitLab import GitLabOAuthToken
from IGitt.GitHub.GitHubOrganization import GitHubOrganization
from IGitt.GitLab.GitLabOrganization import GitLabOrganization

from coala_web.settings import (
    ORG_NAME,
    GITHUB_API_KEY,
    GITLAB_API_KEY,
    )

GITHUB_TOKEN = GitHubToken(GITHUB_API_KEY)


# Get PyGithub org object to query the GitHub APIs
# with the methods available in PyGithub
def get_org():
    github_obj = Github(GITHUB_API_KEY)
    org = github_obj.get_organization(ORG_NAME)
    return org


# Get IGitt org object to query the GitHub and GitLab
# APIs with the methods available in IGitt.
def get_igitt_org(hoster):
    if hoster == 'github':
        org = GitHubOrganization(GitHubToken(GITHUB_API_KEY), ORG_NAME)
    elif hoster == 'gitlab':
        org = GitLabOrganization(GitLabOAuthToken(GITLAB_API_KEY), ORG_NAME)
    return org


def _validate_username(username, regex, min, max):
    """
    Check if the username is valid or invalid.

    :param username: a string represents username of the user,
                     e.g. 'sks444'
    :param regex: a regular expression to match the username
    :param min: minimum length of the username
    :param max: maximum length of the username
    :return: a boolean value, e.g. True if username is valid
    """
    search = re.compile(regex).search
    length = len(username)
    if not (min <= length <= max):
        return False
    if bool(search(username)):
        return False
    return True


def valid_github_username(username):
    """
    Check if the GitHub username is valid or invalid.

    :param username: GitHub username of the user
    :return:a boolean value, e.g. True if username is valid
    """
    return _validate_username(username, r'[^a-zA-Z0-9-]', 1, 39)
